import json

from graph import Graph
from utils import datetime_to_secs


class Job:
    """
    TODO
    """
    def __init__(self, json_path):
        """
        TODO
        """
        with open(json_path) as json_file:
            job_dict = json.load(json_file)['nodes']

        nb_tasks = len(job_dict.keys())
        self.dependence_graph = Graph(nb_tasks)
        self.durations = {}

        for u in job_dict.keys():
            for v in job_dict[u]['Dependencies']:
                self.dependence_graph.add_edge(u, v)

            self.durations[u] = datetime_to_secs(job_dict[u]['Data'])
