from graph import *
import networkx as nx

def compute_rank(graph, i, durations, ranks):
    neighbors = list(graph.neighbors(i))
    if len(neighbors) == 0:
        ranks[i] = (durations[i], i)
    else:
        rank = 0
        for j in neighbors:
            compute_rank(graph, j, durations, ranks)
            rank = max(rank, ranks[j][1])
        ranks[i] = (rank + durations[i], i)

def heft(nb_tasks, dependances, durations, nb_proc):
    graph = nx.DiGraph()
    graph.add_nodes_from(range(nb_tasks))
    graph.add_edges_from(dependances)
    
    ranks = [-1]*nb_tasks

    for i in range(nb_tasks):
        if ranks[i] == -1:
            compute_rank(graph, i, durations, ranks)

    ranks.sort(reverse=True)
    order = [i for _, i in ranks]

    availability_proc = [0]*nb_proc
    availability_tasks = [0]*nb_tasks
    res = []

    for i in order:
        p = 0
        earliest_finish_time = float('inf')
        for k in range(nb_proc):
            finish_time = max(availability_proc[k], availability_tasks[i]) + durations[i]
            if finish_time < earliest_finish_time:
                earliest_finish_time = finish_time
                p = k
        
        res.append((i, p, max(availability_proc[p], availability_tasks[i]), max(availability_proc[p], availability_tasks[i]) + durations[i]))

        availability_proc[p] = earliest_finish_time
        for j in graph.neighbors(i):
            availability_tasks[j] = max(earliest_finish_time, availability_tasks[j])
    
    return res
