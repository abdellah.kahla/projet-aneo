def datetime_to_secs(datetime: str):
    hms_list = datetime.split(':')
    
    secs = 0.
    for i, t in enumerate(hms_list):
        secs += (60. ** (2 - i)) * float(t)

    return secs

