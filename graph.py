from collections import defaultdict

class Graph:
    def __init__(self, vertices):
        self.graph = defaultdict(list)
        self.V = vertices
        
    def add_edge(self, u, v):
        self.graph[u].append(v)
    
    def topological_sort(self):
        print(self.graph)
        in_degree = [0]*(self.V)
        visited = [0]*(self.V)
        for i in self.graph:
            for j in self.graph[i]:
                in_degree[j] += 1
        top_order = []
        queue = []
        from heapq import heappush, heappop
        for i in range(self.V):
            if in_degree[i] == 0:
                heappush(queue, i)
        
        while queue:
            u = heappop(queue)
            if not visited[u]:
                top_order.append(u)
                for i in self.graph[u]:
                    in_degree[i] -= 1
                    if in_degree[i] == 0:
                        heappush(queue, i)
                visited[u] = 1
        
        return top_order
